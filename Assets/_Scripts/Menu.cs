﻿using UnityEngine;

public static class Menu{

	public static void pauseorunpause(){
		if (Time.timeScale == 1){
			pause();
		}
		else{
			if (Master.MASTER.MENU_GAMEOVER.activeInHierarchy){
				restart();
			}
			else if (Master.MASTER.MENU_NEXTLEVEL.activeInHierarchy){
				win();
			}
			resume();
		}
	}

	public static void pause(){
		Time.timeScale = 0;

		Master.MASTER.MENU_BORDER.SetActive(true);
		Master.MASTER.MENU_PAUSE.SetActive(true);
	}

	public static void gameOver(){
		Time.timeScale = 0;

		Master.MASTER.MENU_BORDER.SetActive(true);
		Master.MASTER.MENU_GAMEOVER.SetActive(true);
	}

	public static void finishLevel(){
		Time.timeScale = 0;

		Master.MASTER.MENU_BORDER.SetActive(true);
		Master.MASTER.MENU_NEXTLEVEL.SetActive(true);
	}

	public static void restart(){
		Master.MASTER.restart();
		resume();
	}

	public static void win(){
		Master.reset();
		resume();
	}

	public static void resume(){
		Master.MASTER.MENU_BORDER.SetActive(false);
		Master.MASTER.MENU_GAMEOVER.SetActive(false);
		Master.MASTER.MENU_PAUSE.SetActive(false);
		Master.MASTER.MENU_NEXTLEVEL.SetActive(false);

		foreach(GameObject Player in Master.AllPlayers){
			Player.SendMessage("halt");
		}

		Time.timeScale = 1;
	}
}
