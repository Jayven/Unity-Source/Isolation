﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Master : MonoBehaviour {
	public static Master MASTER;
	public GameObject MENU_GAMEOVER;
	public GameObject MENU_BORDER;
	public GameObject MENU_PAUSE;
	public GameObject MENU_NEXTLEVEL;
	public GameObject HUD_Lives;
	public GameObject HUD_Level;
	public GameObject HUD_Cleared;
	public GameObject HUD_Goal;
	public GameObject BLOCK_PREFAB;
	public GameObject SIMPLE_BLOCK_PREFAB;
	public GameObject PLAYER_PREFAB;
	public GameObject ENEMY_1_PREFAB;
	public GameObject ENEMY_2_PREFAB;
	public GameObject BLOCKS_GAME_OBJECT;

	public static List<GameObject> AllPlayers = new List<GameObject>();
	public static List<GameObject> AllEnemies = new List<GameObject>();
	public static GameObject[,] AllBlocks = new GameObject[40, 28];
	public static int level = 0;

	Vector2 resolution;

	void Start(){
		MASTER = this;
		Time.timeScale = 0;

		for (int i = 0; i < 40; i++){
			for(int k = 0; k < 28; k++){
				if (i < 2 || 37 < i || k < 2 || 25 < k){
					AllBlocks[i, k] = Instantiate(SIMPLE_BLOCK_PREFAB, new Vector3(i, k, 0), Quaternion.identity);
				}
				else{
					AllBlocks[i, k] = Instantiate(BLOCK_PREFAB, new Vector3(i, k, 0), Quaternion.identity);
				}
			}
		}
		
		reset();
		forceCameraSize();
		this.resolution = new Vector2(Screen.width, Screen.height);
	}

	void Update(){
		if (resolution.x != Screen.width || resolution.y != Screen.height){
			this.forceCameraSize();
			this.resolution.x = Screen.width;
			this.resolution.y = Screen.height;
		}
	}

	public void forceCameraSize(){
		Debug.Log("H");
		this.gameObject.GetComponent<Camera>().orthographicSize = Mathf.Max(((float)20 / ((float)Screen.width / (float)Screen.height)), 15);
	}


	public void button_restart(){
		Menu.restart();
	}
	
	public void button_win(){
		Menu.win();
	}

	public void button_resume(){
		Menu.resume();
	}

	public void restart(){
		Time.timeScale = 0;
		level = 0;

		for(int i = 0; i < AllPlayers.Count; i++){
			AllPlayers[i].GetComponent<Player>().restart();
		}

		reset();
	}

	public static void reset(){
		Time.timeScale = 0;

		level++;

		for(int i = 0; i < AllPlayers.Count; i++){
			AllPlayers[i].GetComponent<Player>().reset();
		}

		//Clear all Enemies
		while (AllEnemies.Count != 0){
			UnityEngine.Object.Destroy(AllEnemies[0]);
			AllEnemies.RemoveAt(0);
		}

		//Clear and Create Blocks
		for (int i = 0; i < 40; i++){
			for(int k = 0; k < 28; k++){
				if (2 <= i && i <= 37 && 2 <= k && k <= 25){
				 	Flood.toSea(new int[]{i, k});
				}
			}
		}

		//Instantiate Sea-Enemies / Enemy_1
		for (int i = 0; i < Mathf.Round(Mathf.Sqrt(level) + 1); i++){
			GameObject temp = UnityEngine.Object.Instantiate<GameObject>(Master.MASTER.ENEMY_1_PREFAB, new Vector3(
				(float)(Mathf.Round(Random.value * 35) + 2),
				(float)(Mathf.Round(Random.value * 23) + 2),
				0
				),
				Quaternion.identity
			);
			AllEnemies.Add(temp);
		}
		//Instantiate Land-Enemies / Enemy_2
		for (int i = 0; i < Mathf.Floor((Mathf.Sqrt(level) + 1) / 2); i++){
			GameObject temp = UnityEngine.Object.Instantiate<GameObject>(Master.MASTER.ENEMY_2_PREFAB, new Vector3(
				(float)(Mathf.Round(Random.value * 39)),
				(float)(Random.value > .5? 0: 1),
				0
				),
				Quaternion.identity
			);
			AllEnemies.Add(temp);
		}
		Time.timeScale = 1;
	}
}