﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy1 : MonoBehaviour {
	public float MAX_VELOCITY;

	void Start () {
		// this.GetComponent<Rigidbody2D>().velocity = new Vector3(
		// 	Random.value > .5? MAX_VELOCITY: -MAX_VELOCITY,
		// 	Random.value > .5? MAX_VELOCITY: -MAX_VELOCITY,
		// 	-1
		// );
	}

	void Update(){
		float vel_X = this.GetComponent<Rigidbody2D>().velocity.x;
		if (vel_X != 0){
			vel_X =  Mathf.Sign(vel_X) * MAX_VELOCITY;
		}
		else{
			vel_X = Random.value > .5? MAX_VELOCITY: -MAX_VELOCITY;
		}

		float vel_Y = this.GetComponent<Rigidbody2D>().velocity.y;
		if (vel_Y != 0){
			vel_Y =  Mathf.Sign(vel_Y) * MAX_VELOCITY;
		}
		else{
			vel_Y = Random.value > .5? MAX_VELOCITY: -MAX_VELOCITY;
		}

		this.GetComponent<Rigidbody2D>().velocity = new Vector3(
			vel_X,
			vel_Y,
			-1
		);
	}

	public void OnTriggerEnter2D(Collider2D coll){
		if (coll.gameObject.layer >= 12){
			coll.gameObject.SendMessage("kill", null, SendMessageOptions.RequireReceiver);
		}
	}
}
