﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {
	Queue<GameObject> List_GameObject_Temps = new Queue<GameObject>();

	public float MAX_VELOCITY;

	float lastClick = 0;
	float last_kill_time = 0;

	int filled;
	int goal = 60;
	int lives = 2;

	int axisInUse_X = 0;
	int axisInUse_Y = 0;

	void Start(){
		Master.AllPlayers.Add(this.gameObject);
	}

	void updateHUD(){
		Master.MASTER.HUD_Lives.GetComponent<Text>().text = "Lives: " + lives.ToString();
		Master.MASTER.HUD_Level.GetComponent<Text>().text = "Level: " + Master.level.ToString();
		Master.MASTER.HUD_Cleared.GetComponent<Text>().text = "Cleared: " + (Mathf.Round((float)(filled / 8.64))).ToString() + "%";
		Master.MASTER.HUD_Goal.GetComponent<Text>().text = "Goal: " + goal.ToString() + "%";
	}

	public void kill(){
		this.gameObject.transform.position = new Vector3(19, 27, 0);
		this.GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0, 0);
		
		axisInUse_X = 0;
		axisInUse_Y = 0;
		
		if (last_kill_time < Time.time - 1){
			last_kill_time = Time.time;
			lives--;

			clearTemps();

			if (lives < 0){
				Menu.gameOver();
			}
		}
		updateHUD();
	}

	public void halt(){
		this.GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0, 0);

		this.transform.position = new Vector3(
			Mathf.Round(this.transform.position.x),
			Mathf.Round(this.transform.position.y),
			this.transform.position.z
		);

		axisInUse_X = 0;
		axisInUse_Y = 0;
	}

	public void restart(){
		lives = 3;
		reset();
	}

	public void reset(){
		this.gameObject.transform.position = new Vector3(19, 27, 0);

		filled = 0;
		lives += (int)Mathf.Round(Mathf.Sqrt(Master.level));
		goal = (int)(Mathf.Round(-500 / (Master.level + 10)) + 90);
		updateHUD();
	}

	bool checkwin(){
		if (Mathf.Round((float)(filled / 8.64)) >= goal){
			Menu.finishLevel();
			return true;
		}
		updateHUD();
		return false;
	}

	void Update(){
		if (last_kill_time > Time.time - .1){
			clearTemps();
		}
		if (Input.GetAxisRaw("Horizontal") != 0){
			if (Input.GetAxisRaw("Horizontal") != axisInUse_X){
				axisInUse_X = (int)Input.GetAxisRaw("Horizontal");
				axisInUse_Y = 0;

				moveHorizontal(Input.GetAxisRaw("Horizontal") > 0);
			}
		}

		if (Input.GetAxisRaw("Vertical") != 0){
			if (Input.GetAxisRaw("Vertical") != axisInUse_Y){
				axisInUse_Y = (int)Input.GetAxisRaw("Vertical");
				axisInUse_X = 0;

				moveVertical(Input.GetAxisRaw("Vertical") > 0);
			}  
		}
		
		if (Input.GetButtonDown("Submit")){
			Menu.pauseorunpause();
		}

		if (Input.GetButtonDown("Fire1")){
			if (lastClick < Time.time - .1){
				lastClick = Time.time;
				float pos_X = Input.mousePosition.x - (Screen.width / 2);
				float pos_Y = Input.mousePosition.y - (Screen.height / 2);
				if (Mathf.Abs(pos_X) < 125 && Mathf.Abs(pos_Y) < 125){
					Menu.pauseorunpause();
				}
				else{
					if (Mathf.Abs(pos_X) > Mathf.Abs(pos_Y)){
						moveHorizontal(pos_X > 0);
					}
					else{
						moveVertical(pos_Y > 0);
					}
				}
			}
		}
	}

	void moveHorizontal(bool dir){
		this.GetComponent<Rigidbody2D>().velocity = new Vector3((dir? 1: -1) * MAX_VELOCITY, 0, 0);

		this.transform.position = new Vector3(
			this.transform.position.x,
			Mathf.Round(this.transform.position.y),
			this.transform.position.z
		);
	}
	void moveVertical(bool dir){
		this.GetComponent<Rigidbody2D>().velocity = new Vector3(0, (dir? 1: -1) * MAX_VELOCITY, 0);
		this.transform.position = new Vector3(
			Mathf.Round(this.transform.position.x),
			this.transform.position.y,
			this.transform.position.z
		);
	}
	
	public void OnTriggerEnter2D(Collider2D coll){
		if (coll.gameObject.layer == 8){
			int rounds = 0;
			while (List_GameObject_Temps.Count != 0 ){
				rounds++;
				filled += Flood.flood(List_GameObject_Temps);
				checkwin();
				updateHUD();
			}
		}
		if (coll.gameObject.layer == 9){
			List_GameObject_Temps.Enqueue(coll.gameObject);
			coll.gameObject.GetComponent<Block>().owner = this.gameObject;
			Flood.toTemp(
				new int[]{
					(int)Mathf.Round(coll.gameObject.transform.position.x),
					(int)Mathf.Round(coll.gameObject.transform.position.y)
				}
			);
		}
	}

	void clearTemps(){
		//Clear and Create Blocks
		while (List_GameObject_Temps.Count != 0){
			Flood.toSea(new int[]{
				(int)Mathf.Round(List_GameObject_Temps.Peek().transform.position.x),
				(int)Mathf.Round(List_GameObject_Temps.Dequeue().transform.position.y)				
			});
		}
	}
}
