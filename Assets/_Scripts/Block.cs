﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour {
	public GameObject owner;

	public void kill(){
		owner.SendMessage("kill");
	}
}
