﻿using UnityEngine;
using System.Collections.Generic;

public static class Flood{
	public static Color Orange = new Color(1f, .33f, 0f, 1f);

	public static void toTemp(int[] pos){
		Master.AllBlocks[pos[0], pos[1]].layer = 13;
		Master.AllBlocks[pos[0], pos[1]].GetComponent<SpriteRenderer>().color = Orange;
	}
	public static void toLand(int[] pos){
		Master.AllBlocks[pos[0], pos[1]].layer = 8;
		Master.AllBlocks[pos[0], pos[1]].GetComponent<SpriteRenderer>().color = Color.black;
	}
	public static void toSea(int[] pos){
		Master.AllBlocks[pos[0], pos[1]].layer = 9;
		Master.AllBlocks[pos[0], pos[1]].GetComponent<SpriteRenderer>().color = Color.white;
	}

	public static int flood(Queue<GameObject> AllTemps){
		int filled = 0;

		List<int[]> EnemyPOS = new List<int[]>();
		for (int i = 0; i < Master.AllEnemies.Count; i++){
			EnemyPOS.Add(new int[]{
				(int)Mathf.Round(Master.AllEnemies[i].transform.position.x),
				(int)Mathf.Round(Master.AllEnemies[i].transform.position.y)
			});
		}

		List<int[]> CheckedBulk = new List<int[]>();

		while (AllTemps.Count != 0){
			int[] temp = new int[2]{
				(int)Mathf.Round(AllTemps.Peek().transform.position.x), 
				(int)Mathf.Round(AllTemps.Dequeue().transform.position.y)
			};

			filled++;
			toLand(new int[]{temp[0], temp[1]});

			if (fork(leftof(temp), CheckedBulk, EnemyPOS)){
				filled += fill(leftof(temp));
			}
			if (fork(rightof(temp), CheckedBulk, EnemyPOS)){
				filled += fill(rightof(temp));
			}
			if (fork(upof(temp), CheckedBulk, EnemyPOS)){
				filled += fill(upof(temp));
			}
			if (fork(downof(temp), CheckedBulk, EnemyPOS)){
				filled += fill(downof(temp));
			}
		}

		return filled;
	}


	//Returns if seablock group is free of enemies
	//Helper for recursive function
	static bool fork(int[] thisBlock, List<int[]> CheckedBulk, List<int[]> EnemyPOS){

		//Holds this run's check blocks
		List<int[]> LocalChecked = new List<int[]>();

		bool safe = fork(thisBlock, LocalChecked, CheckedBulk, EnemyPOS);

		if (!safe){
			//If they succeeded, it's now land and will be rejected already
			CheckedBulk.AddRange(LocalChecked);
		}

		return safe;
	}

	//Returns if seablock group is free of enemies
	static bool fork(int[] thisBlock, List<int[]> checkedBlocks, List<int[]> CheckedBulk, List<int[]> EnemyPOS){
		//Checked bulk only contains checked blocks from previous runs
		//If it was checked and not completed in a previous run, no point in checking to fail this run
		for(int i = 0; i < CheckedBulk.Count; i++){
			if (CheckedBulk[i][0] == thisBlock[0] && CheckedBulk[i][1] == thisBlock[1]){
				return false;
			}
		}

		//If this block has already been checked locally, return true
		for(int i = 0; i < checkedBlocks.Count; i++){
			if (checkedBlocks[i][0] == thisBlock[0] && checkedBlocks[i][1] == thisBlock[1]){
				return true;
			}
		}

		//If this block is not sea, return true
		if((Master.AllBlocks[thisBlock[0], thisBlock[1]]).layer != 9){
			return true;
		}

		//Add this block to checkedList
		//Adding land blocks to checked will cause bug (hence this is after sea check)
		//Because next fork run will include land block
		//That may be part of two sea groups
		checkedBlocks.Add(thisBlock);

		//if this block collides with an enemy, return false
		foreach(int[] enemy in EnemyPOS){
			if (enemy[0] == thisBlock[0] && enemy[1] == thisBlock[1]){
				return false;
			}
		}

		//This block is a unchecked, safe, sea block, so check all surrounding
		return (
			fork(leftof(thisBlock), checkedBlocks, CheckedBulk, EnemyPOS) &&
			fork(rightof(thisBlock), checkedBlocks, CheckedBulk, EnemyPOS) &&
			fork(downof(thisBlock), checkedBlocks, CheckedBulk, EnemyPOS) &&
			fork(upof(thisBlock), checkedBlocks, CheckedBulk, EnemyPOS)
		);
	}

	static int fill(int[] thisBlock){
		if (Master.AllBlocks[thisBlock[0], thisBlock[1]].layer == 9){
			toLand(thisBlock);
			return 1 + 
				fill(leftof(thisBlock)) +
				fill(downof(thisBlock)) +
				fill(rightof(thisBlock)) +
				fill(upof(thisBlock));
		}
		else{
			return 0;
		}
	}

	static int[] leftof(int[] pos){
		//Useds AllBlocks 2D-list to return block left of param
		return new int[]{pos[0] - 1, pos[1]};
	}
	static int[] rightof(int[] pos){
		//Useds AllBlocks 2D-list to return block right of param
		return new int[]{pos[0] + 1, pos[1]};
	}
	static int[] upof(int[] pos){
		//Useds AllBlocks 2D-list to return block above param
		return new int[]{pos[0], pos[1] + 1};
	}
	static int[] downof(int[] pos){
		//Useds AllBlocks 2D-list to return block below param
		return new int[]{pos[0], pos[1] - 1};
	}
}